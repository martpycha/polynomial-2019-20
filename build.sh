#!/bin/sh

set -xue

mkdir -p "out/main"
mkdir -p "out/test"

javac -d out/main $( find src/main/ -name '*.java')
javac -d out/test -cp out/main:lib/junit-4.13.jar $( find src/test/ -name '*.java')

java -cp out/main:out/test:lib/junit-4.13.jar:lib/hamcrest-core-1.3.jar \
    org.junit.runner.JUnitCore PolynomialTest
