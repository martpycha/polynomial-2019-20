ublic class Main {
    public static void main(String[] args) {
        if (args.length < 4) {
            printHelp();
            return;
        }

        String op = args[0];

        int delimPosition = 0;
        for (int i = 1; i < args.length; i++) {
            if (args[i].equals("--")) {
                delimPosition = i;
                break;
            }
        }
        if (delimPosition == 0) {
            printHelp();
            return;
        }

        int[] coefFirst = parseCoefficients(args, 1, delimPosition);
        int[] coefSecond = parseCoefficients(args, delimPosition + 1, args.length);

        if ((coefFirst.length == 0) || (coefSecond.length == 0)) {
            printHelp();
            return;
        }

        Polynomial first = new Polynomial(coefFirst);
        Polynomial second = new Polynomial(coefSecond);

        Polynomial result;
        String opSymbol;
        if (op.equals("plus")) {
            result = Polynomial.sum(first, second);
            opSymbol = "+";
        } else if (op.equals("times")) {
            result = Polynomial.product(first, second);
            opSymbol = "*";
        } else if (op.equals("divide")) {
            result = Polynomial.div(first, second);
            opSymbol = ":";
        } else {
            printHelp();
            return;
        }
        if (op.equals("divide")) {
        	if (coefSecond[0] == 0)  {
        		printHelp();
        	}  else  {
        		Polynomial remainder;
        		remainder = Polynomial.mod(first, second);
        		System.out.printf("(%s) %s (%s) = (%s) (%s)\n",
        				first.toPrettyString("x"),
        				opSymbol,
        				second.toPrettyString("x"),
        				result.toPrettyString("x"),
        				remainder.toPrettyString("x"));
        	}
        }  else  {
        	System.out.printf("(%s) %s (%s) = (%s)\n",
                first.toPrettyString("x"),
                opSymbol,
                second.toPrettyString("x"),
                result.toPrettyString("x"));
        }
    }

    private static void printHelp() {
        System.out.println("Usage: This program computes with two polynomials.");
        System.out.println("You can use three operations: sum (plus), product (times) and division (divide).");
        System.out.println("Use '--' to differentiate between two polynomials.");
        System.out.println("Form: 'operation' 'first polynomial' '--' 'second polynomial'");
        System.out.println("Examples:");
        System.out.println("plus 1 -- 1 (same as 1 + 1)");
        System.out.println("times 2 3 -- -5 10 (same as (2x + 3) * (-5x + 10))");
        System.out.println("divide 10 -8 9 -- 2 (same as (10x^2 - 8x + 9) : 2)");
        System.out.println("Happy usage!");
    }

    private static int[] parseCoefficients(String[] args, int firstIndexIncl, int lastIndexExcl) {
        int[] result = new int[lastIndexExcl - firstIndexIncl];
        int resIndex = result.length - 1;
        for (int i = firstIndexIncl; i < lastIndexExcl; i++) {
            result[resIndex] = Integer.parseInt(args[i]);
            resIndex--;
        }
        return result;
    }

}
