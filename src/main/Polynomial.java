import java.util.Arrays;

/** Integer-only polynomials. */
public class Polynomial {
	private int[] coefficients;

    /** Create new instance with given coefficients.
     *
     * x^2 + 3x - 7 would be created by new Polynomial(-7, 3, 1)
     * x^5 + 42 would be created by new Polynomial(42, 0, 0, 0, 0, 1)
     *
     * @param coef Coefficients, ordered from lowest degree (power).
     */
    public Polynomial(int... coef) {
        System.out.printf("Creating polynomial");
        coefficients = new int[coef.length];
        for (int i = 0; i < coef.length; i++) {
        	coefficients[i] = coef[i];
            if (i > 0) {
                System.out.print(" +");
            }
            System.out.printf(" %d * x^%d", coef[i], i);
        }
        System.out.println(" ...");
    }

    /** Get coefficient value for given degree (power). */
    public int getCoefficient(int deg) {
        if (coefficients.length - 1 < deg) {
        	return 0;
        } else {
        	return coefficients[deg];
        }
    }

    /** Get degree (max used power) of the polynomial. */
    public int getDegree() {
    	int degree = coefficients.length - 1;
    	if (degree == -1) {
    		return 0;
    	} else {
        return degree;
    	}
    }

    /** Format into human-readable form with given variable. */
    public String toPrettyString(String variable) {
    	int degree = coefficients.length - 1;
    	String prettypolynomial = "";
    	if (degree > 0)  {
    		for (int i = degree; i >= 0; i--)  {
    			String variousvariable = "";
    			String prettynomial;
    			if (i == 1)  {
    				variousvariable = variable;
    			} else if (i == 0)  {
    				variousvariable = "";
    			} else  {
    				variousvariable = String.format(variable + "^%d", i);
    			}
    			if (coefficients[i] == 1)  {
    				if (i == 0)  {
    					prettynomial = "1";
    				} else {
    					prettynomial = String.format("%s", variousvariable);
    				}
    			} else if (coefficients[i] == 0)  {
    				prettynomial = "";
    			} else {
    				prettynomial = String.format("%d%s", Math.abs(coefficients[i]), variousvariable);
    			}
    			if (coefficients[i] < 0 && degree == i)  {
    		    	prettypolynomial = "-" + prettynomial; 
    			} else if (coefficients[i] < 0 && degree != i)  {
    				prettypolynomial = prettypolynomial + " - " + prettynomial; 
    			} else if (coefficients[i] > 0 && degree == i)  {
    				prettypolynomial = prettynomial;
    			}  else if (coefficients[i] != 0) {
    				prettypolynomial = prettypolynomial + " + " + prettynomial;
    			} 
    		} 
    		
    	}
    	System.out.printf("%s\n\n", prettypolynomial);
    	return prettypolynomial;
    	
    }

    /** Debugging output, dump only coefficients. */
    @Override
    public String toString() {
    	int number = coefficients.length - 1;
    	String toString = "";
    	for (int i = 0; i <= number; i++)  {
    		if (i == 0)  {
    			toString = "[" + coefficients[i]; 
    			if (coefficients.length == 1)  {
    				toString = toString + "]";
    			}
     		} else if (i == number)  {
     			toString = toString + "," + coefficients[i] + "]";
     		} else {	
     			toString = toString + "," +  coefficients[i];
     		}
    	}
    	String finishedtoString = "Polynomial" + toString;
    	System.out.printf("%s\n\n", finishedtoString);
    	return finishedtoString; 
    }

    /** Adds together given polynomials, returning new one. */
    public static Polynomial sum(Polynomial ... polynomials) {
    	int majorDegree;
    	int minorDegree;
    	boolean firstIsBigger = polynomials[0].getDegree() > polynomials[1].getDegree();
    	if (firstIsBigger)  {
    		majorDegree = polynomials[0].getDegree();
    		minorDegree = polynomials[1].getDegree();
    	}  else  {
    		majorDegree = polynomials[1].getDegree();
    		minorDegree = polynomials[0].getDegree();
    	}
    	int difference = majorDegree - minorDegree;
    	int[] sumPolynomial = new int[majorDegree + 1];
    	for (int i = 0; i <= majorDegree; i++)  {
    		if (firstIsBigger && majorDegree - difference < i)  {
    			sumPolynomial[i] = polynomials[0].getCoefficient(i);
    		}  else if (!firstIsBigger && majorDegree - difference < i && i != 0) {
    			sumPolynomial[i] = polynomials[1].getCoefficient(i);
    		}  else  {
    			sumPolynomial[i] = polynomials[0].getCoefficient(i) + polynomials[1].getCoefficient(i);
    		}
    	}
    	Polynomial result = new Polynomial(sumPolynomial);
    	for (int element : sumPolynomial)  {
    		System.out.println(element);
    		System.out.println("saaakrys");
    	}
    	return result;
    	
    }

    /** Multiplies together given polynomials, returning new one. */
    public static Polynomial product(Polynomial... polynomials) {
        int minorDegree;
        int majorDegree;
        boolean firstIsBigger = polynomials[0].getDegree() > polynomials[1].getDegree();
        if (firstIsBigger)  {
    		majorDegree = polynomials[0].getDegree();
    		minorDegree = polynomials[1].getDegree();
    	}  else  {
    		majorDegree = polynomials[1].getDegree();
    		minorDegree = polynomials[0].getDegree();
    	}
        int[] finalProductPolynomial = new int[majorDegree + minorDegree + 1];
        int[] productPolynomial = new int[majorDegree + minorDegree + 1];
        for (int y = 0; y <= majorDegree; y++)  {
        	for (int i = 0; i <= minorDegree; i++)  {
        		if (firstIsBigger)  {
        			productPolynomial[y + i] = polynomials[0].getCoefficient(y) * polynomials[1].getCoefficient(i);
        		} else {
        			productPolynomial[y + i] = polynomials[1].getCoefficient(y) * polynomials[0].getCoefficient(i);
        		}        		
        	finalProductPolynomial[y + i] = finalProductPolynomial[y + i] + productPolynomial[y + i];
        	}
        }
        Polynomial product = new Polynomial(finalProductPolynomial);
        for (int element : finalProductPolynomial)  {
        	System.out.println(element);
        	System.out.println("saaakrauuz");
        }
        System.out.printf("Majordegreeis%d", majorDegree);
        System.out.printf("Minordegreeis%d", minorDegree);
        return product;
    	// return new Polynomial(0);
    }

    /** Get the result of division of two polynomials, ignoring remaineder. */
    public static Polynomial div(Polynomial dividend, Polynomial divisor) {
    	boolean divideByZero = divisor.getCoefficient(0) == 0 && divisor.getDegree() == 0;
    	int dividendDegree = dividend.getDegree();
    	int divisorDegree = divisor.getDegree();
    	int[] result = new int[dividendDegree - divisorDegree + 1];
    	int[] newDividend = new int[dividendDegree + 1];
    	for (int i = 0; i <= dividend.getDegree(); i++)  {
    		newDividend[i] = dividend.getCoefficient(i);
    	}
    	int resultPosition = dividendDegree - divisorDegree;
     	for (int i = dividendDegree; i >= 0; i--)  {
    		
    		if (divideByZero)  {
    			break;
    		}
    		if (resultPosition < 0)  {
    			break;
     		}  else if (newDividend[i] % (divisor.getCoefficient(divisorDegree)) == 0)  {
    			// division of the dividend by divisor
    			int step = (newDividend[i] / (divisor.getCoefficient(divisorDegree)));
    			
    			int[] middleStep = new int[divisorDegree + 1];
    			for (int y = divisorDegree; y >= 0; y--)  {
    				// multiplying the divisor polynomial by the result of the first division
    				middleStep[y] = step * divisor.getCoefficient(y);
    			}
    			int positionWantedForChange = i;
    			int middleStepDegree = middleStep.length - 1;
    			for (int j = middleStepDegree; j >= 0; j--)  {
    				// sustriction of the middleStep from the remaining polynomial (dividend)
    				newDividend[positionWantedForChange] = newDividend[positionWantedForChange] - middleStep[j];
    				positionWantedForChange--;
    			}
    			
    			//adding the result into the array
    			result[resultPosition] = step;	
    			resultPosition--;

    		}

    	}
     	if (divideByZero)  {
     		return new Polynomial ();
     	} else {
     	System.out.println("VYSLEDEK DELENI");

     	System.out.print(result);
     	
     	return new Polynomial (result);
		}
    }

    /** Get the remainder of division of two polynomials. */
    public static Polynomial mod(Polynomial dividend, Polynomial divisor) {
    	boolean divideByZero = divisor.getCoefficient(0) == 0 && divisor.getDegree() == 0;
    	int dividendDegree = dividend.getDegree();
    	int divisorDegree = divisor.getDegree();
    	int[] result = new int[dividendDegree - divisorDegree + 1];
    	int[] newDividend = new int[dividendDegree + 1];
		int[] middleStep = new int[divisorDegree + 1];
    	for (int i = 0; i <= dividend.getDegree(); i++)  {
    		newDividend[i] = dividend.getCoefficient(i);
    	}
    	int resultPosition = dividendDegree - divisorDegree;
     	for (int i = dividendDegree; i >= 0; i--)  {
    		
     		if (divideByZero)  {
    			break;
    		}
    		if (resultPosition < 0)  {
    			break;
     		}  else if (newDividend[i] % (divisor.getCoefficient(divisorDegree)) == 0)  {
    			// division of the dividend by divisor
    			int step = (newDividend[i] / (divisor.getCoefficient(divisorDegree)));
    			
    			for (int y = divisorDegree; y >= 0; y--)  {
    				// multiplying the divisor polynomial by the result of the first division
    				middleStep[y] = step * divisor.getCoefficient(y);
    			}
    			int positionWantedForChange = i;
    			int middleStepDegree = middleStep.length - 1;
    			for (int j = middleStepDegree; j >= 0; j--)  {
    				// sustriction of the middleStep from the remaining polynomial (dividend)
    				newDividend[positionWantedForChange] = newDividend[positionWantedForChange] - middleStep[j];
    				positionWantedForChange--;
    			}
    			
    			//adding the result into the array
    			result[resultPosition] = step;	
    			resultPosition--;

    		}
    	}
     	int lengthOfMod = 0;
		for (int n = 0; n < newDividend.length; n++)  {
			if (newDividend[n] != 0)  {
				lengthOfMod++;
			} else if (newDividend[0] == 0)  {
				lengthOfMod = 1;
			}
		}
		int[] mod = new int [lengthOfMod];
		for (int k = 0; k < lengthOfMod; k++)  {
				mod[k] = newDividend[k];
		}
		if (divideByZero)  {
			return new Polynomial();
		} else {
			System.out.println("ZBYTEK DELENI");
			System.out.print(mod);
			System.out.printf("%d", lengthOfMod);
			System.out.printf("%d", newDividend.length);
			System.out.print(mod);
     	
     	return new Polynomial(mod);
		}
    }
    
}
//S metodou toPrettyString a částečně i s metodou sum mi pomohl Petr Novák