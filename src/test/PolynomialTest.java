import static org.junit.Assert.*;
import org.junit.Test;

public class PolynomialTest {
    @Test
    public void getDegreeEmpty() {
        Polynomial a = new Polynomial();
        assertEquals(0, a.getDegree());
    }

    @Test
    public void getDegreeNumber() {
        Polynomial a = new Polynomial(42);
        assertEquals(0, a.getDegree());
    }

    @Test
    public void getDegreeCubic() {
        Polynomial a = new Polynomial(7, 8, 9, 10);
        assertEquals(3, a.getDegree());
    }

    @Test
    public void getCoefficientQuadratic() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals(7, a.getCoefficient(0));
        assertEquals(8, a.getCoefficient(1));
        assertEquals(9, a.getCoefficient(2));
    }

    @Test
    public void getCoefficientOutOfRange() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals(0, a.getCoefficient(3));
        assertEquals(0, a.getCoefficient(4));
    }

    @Test
    public void toStringNumber() {
        Polynomial a = new Polynomial(42);
        assertEquals("Polynomial[42]", a.toString());
    }

    @Test
    public void toStringQuadratic() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals("Polynomial[7,8,9]", a.toString());
    }

    @Test
    public void toPrettyStringQuadratic() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals("9x^2 + 8x + 7", a.toPrettyString("x"));
    }

    @Test
    public void toPrettyStringQuadraticWithY() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals("9y^2 + 8y + 7", a.toPrettyString("y"));
    }

    @Test
    public void toPrettyStringWithOne() {
        Polynomial a = new Polynomial(1, 2, 1);
        assertEquals("x^2 + 2x + 1", a.toPrettyString("x"));
    }

    @Test
    public void toPrettyStringWithZero() {
        Polynomial a = new Polynomial(3, 0, 5);
        assertEquals("5x^2 + 3", a.toPrettyString("x"));
    }

    @Test
    public void toPrettyStringWithNegative() {
        Polynomial a = new Polynomial(17, 0, -2, -5);
        assertEquals("-5x^3 - 2x^2 + 17", a.toPrettyString("x"));
    }
    @Test
    public void getSumPositive() {
        Polynomial a = new Polynomial(3, 0, 5);
        Polynomial b = new Polynomial(8, 2, 4);
        Polynomial test = Polynomial.sum(a, b);
        assertEquals("Polynomial[11,2,9]", test.toString());
    }
    @Test
    public void getSumNegative() {
        Polynomial a = new Polynomial(3, 0, -5);
        Polynomial b = new Polynomial(-8, 2, 4);
        Polynomial test = Polynomial.sum(a, b);
        assertEquals("Polynomial[-5,2,-1]", test.toString());
    }
    @Test
    public void getSumDifferentLength() {
        Polynomial a = new Polynomial(0, -5);
        Polynomial b = new Polynomial(8, 2, 4, 15);
        Polynomial test = Polynomial.sum(a, b);
        assertEquals("Polynomial[8,-3,4,15]", test.toString());
    }
    @Test
    public void getProductPositive() {
        Polynomial a = new Polynomial(2, 4, 5);
        Polynomial b = new Polynomial(8, 2, 1);
        Polynomial test = Polynomial.product(a, b);
        assertEquals("Polynomial[16,36,50,14,5]", test.toString());
    }
    @Test
    public void getProductNegative() {
        Polynomial a = new Polynomial(-2, 4, 2);
        Polynomial b = new Polynomial(-3, -9, 1);
        Polynomial test = Polynomial.product(a, b);
        assertEquals("Polynomial[6,6,-44,-14,2]", test.toString());
    } 
    @Test
    public void getProductDifferentLength() {
        Polynomial a = new Polynomial(-2, 0, 2);
        Polynomial b = new Polynomial(-3, 1);
        Polynomial test = Polynomial.product(a, b);
        assertEquals("Polynomial[6,-2,-6,2]", test.toString());
    }
    @Test
    public void division() {
        Polynomial a = new Polynomial(2, 3, 1);
        Polynomial b = new Polynomial(2, 1);
        Polynomial test = Polynomial.div(a, b);
        assertEquals("Polynomial[1,1]", test.toString());
    }
    @Test
    public void divisionAnother() {
        Polynomial a = new Polynomial(3, 13, 7, 1);
        Polynomial b = new Polynomial(3, 1);
        Polynomial test = Polynomial.div(a, b);
        assertEquals("Polynomial[1,4,1]", test.toString());
    }
    @Test
    public void divisionZeros() {
        Polynomial a = new Polynomial(0, 2, 1);
        Polynomial b = new Polynomial(0, 1);
        Polynomial test = Polynomial.div(a, b);
        assertEquals("Polynomial[2,1]", test.toString());
    }
    @Test
    public void divisionMoreZeros() {
        Polynomial a = new Polynomial(0, 0, 0, 1);
        Polynomial b = new Polynomial(0, 1);
        Polynomial test = Polynomial.div(a, b);
        assertEquals("Polynomial[0,0,1]", test.toString());
    }
    @Test
    public void divisionNegative() {
        Polynomial a = new Polynomial(25, -26, 1);
        Polynomial b = new Polynomial(-1, 1);
        Polynomial test = Polynomial.div(a, b);
        assertEquals("Polynomial[-25,1]", test.toString());
    }
    @Test
    public void divisionMoreNegativet() {
        Polynomial a = new Polynomial(-13, -11, 2);
        Polynomial b = new Polynomial(-1, -1);
        Polynomial test = Polynomial.div(a, b);
        assertEquals("Polynomial[13,-2]", test.toString());
    }
    @Test
    public void divisionWithRemainder() {
        Polynomial a = new Polynomial(-1, 3, 1);
        Polynomial b = new Polynomial(1, 1);
        Polynomial test = Polynomial.div(a, b);
        assertEquals("Polynomial[2,1]", test.toString());
    }
    @Test
    public void divisionByZero() {
        Polynomial a = new Polynomial(-1, 3, 1);
        Polynomial b = new Polynomial(0);
        Polynomial test = Polynomial.div(a, b);
        assertEquals("Polynomial", test.toString());
    }
    @Test
    public void mod() {
        Polynomial a = new Polynomial(-1, 3, 1);
        Polynomial b = new Polynomial(1, 1);
        Polynomial test = Polynomial.mod(a, b);
        assertEquals("Polynomial[-3]", test.toString());
    }
    @Test
    public void modMoreNegative() {
        Polynomial a = new Polynomial(-13, -11, 2);
        Polynomial b = new Polynomial(-1, -1);
        Polynomial test = Polynomial.mod(a, b);
        assertEquals("Polynomial[0]", test.toString());
    }
    @Test
    public void modVeryLong() {
        Polynomial a = new Polynomial(7, 0, 0, 0, 19, 0, 0, 6);
        Polynomial b = new Polynomial(5, 0, 0, 3);
        Polynomial test = Polynomial.mod(a, b);
        assertEquals("Polynomial[7,-15]", test.toString());
    }
    @Test
    public void modDivisionByZero() {
        Polynomial a = new Polynomial(7, 0, 0, 0, 19, 0, 0, 6);
        Polynomial b = new Polynomial(0);
        Polynomial test = Polynomial.mod(a, b);
        assertEquals("Polynomial", test.toString());
    }
}
